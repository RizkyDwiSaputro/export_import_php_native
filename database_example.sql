--
-- PostgreSQL database dump
--

-- Dumped from database version 14.3
-- Dumped by pg_dump version 14.3

-- Started on 2022-12-20 16:08:45

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE export_import_excell;
--
-- TOC entry 3311 (class 1262 OID 207345)
-- Name: export_import_excell; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE export_import_excell WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'English_Indonesia.1252';


ALTER DATABASE export_import_excell OWNER TO postgres;

\connect export_import_excell

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 3312 (class 0 OID 0)
-- Dependencies: 3
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 209 (class 1259 OID 215537)
-- Name: data_excell; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.data_excell (
    id integer NOT NULL,
    nama character varying,
    alamat character varying,
    anggaran character varying,
    keterangan text
);


ALTER TABLE public.data_excell OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 215540)
-- Name: data_excell_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.data_excell_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.data_excell_id_seq OWNER TO postgres;

--
-- TOC entry 3313 (class 0 OID 0)
-- Dependencies: 210
-- Name: data_excell_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.data_excell_id_seq OWNED BY public.data_excell.id;


--
-- TOC entry 3164 (class 2604 OID 215541)
-- Name: data_excell id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.data_excell ALTER COLUMN id SET DEFAULT nextval('public.data_excell_id_seq'::regclass);


--
-- TOC entry 3304 (class 0 OID 215537)
-- Dependencies: 209
-- Data for Name: data_excell; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.data_excell VALUES (5, 'Manusia', 'Planet', 'sedang membayar', '1,000,000');
INSERT INTO public.data_excell VALUES (6, 'Ultraman', 'Planet M78', 'baru otw', '10,000,000');


--
-- TOC entry 3314 (class 0 OID 0)
-- Dependencies: 210
-- Name: data_excell_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.data_excell_id_seq', 6, true);


-- Completed on 2022-12-20 16:08:51

--
-- PostgreSQL database dump complete
--

