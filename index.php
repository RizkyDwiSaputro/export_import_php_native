<?php 
    include 'control.php';

    // ambil data dari database
    $query_data_excell = pg_query($conn, "SELECT * FROM public.data_excell");
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>System Export Import Excell</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    </head>
    <body>
        <section>
            <div class="container mt-5">
                <div class="row mt-5">
                    <div class="col-md-12 mt-5">
                        <!-- Button trigger modal Import -->
                        <button type="button" class="btn btn-primary mb-3" data-bs-toggle="modal" data-bs-target="#modal-import">
                            Import
                        </button>
                
                        <!-- Modal Import -->
                        <div class="modal fade import" id="modal-import" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h1 class="modal-title fs-5" id="h1-import">Import Excell</h1>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <form action="control.php" method="POST" enctype="multipart/form-data">
                                        <div class="modal-body">
                                            <div class="mb-3">
                                                <label for="input_import_excell" class="form-label">Masukkan File Excell-nya</label>
                                                <input class="form-control form-control" id="input_import_excell" name="input_import_excell" type="file" required>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Kembali</button>
                                            <button type="submit" name="import_data" class="btn btn-primary">Import</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!-- Button trigger modal Export -->
                        <button type="button" class="btn btn-primary mb-3" data-bs-toggle="modal" data-bs-target="#modal-Export">
                            Export
                        </button>
                
                        <!-- Modal Export -->
                        <div class="modal fade Export" id="modal-Export" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h1 class="modal-title fs-5" id="h1-Export">Export Excell</h1>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <form action="control.php" method="POST" enctype="multipart/form-data">
                                        <div class="modal-body">
                                            <div class="mb-3">
                                                <select class="form-select" aria-label="Default select example" name="select_export" id="select_export" style="cursor: pointer;">
                                                    <option selected>Pilih Type File</option>
                                                    <option value="Xls">Xls</option>
                                                    <option value="Csv">Csv</option>
                                                    <option value="Xlsx">Xlsx</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Kembali</button>
                                            <button type="submit" name="export_data" id="tombol_export" class="btn btn-primary">Export</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <?php if(isset($_SESSION['message'])) : ?>

                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <?php 
                                    echo $_SESSION['message'];
                                    unset($_SESSION['message']);
                                ?>
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>

                            <?php elseif(isset($_SESSION['message_success'])) : ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php 
                                        echo $_SESSION['message_success'];
                                        unset($_SESSION['message_success']);
                                    ?>
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                        <?php endif; ?>

                        <!-- Bagian Table -->
                        <table class="table table-striped table-responsive">
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Nama</th>
                                    <th scope="col">Alamat</th>
                                    <th scope="col">Keterangan</th>
                                    <th scope="col">Anggaran</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; ?>
                                <?php while($data_excell = pg_fetch_object($query_data_excell)) : ?>
                                    <tr>
                                        <th scope="row">
                                            <?= $no++; ?>
                                        </th>
                                        <td>
                                            <?= $data_excell->nama; ?>
                                        </td>
                                        <td>
                                            <?= $data_excell->alamat; ?>
                                        </td>
                                        <td>
                                            <?= $data_excell->keterangan; ?>
                                        </td>
                                        <td>
                                            <?= $data_excell->anggaran; ?>
                                        </td>
                                    </tr>
                                <?php endwhile; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>


        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
        
    </body>
</html>