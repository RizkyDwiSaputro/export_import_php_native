<?php
session_start();
include 'koneksi.php';

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use PhpOffice\PhpSpreadsheet\Writer\Xls;

// Untuk Bagian Import
if(isset($_POST['import_data'])) {

    $ambil_input_file = $_FILES['input_import_excell']['name'];
    $info_path = pathinfo($ambil_input_file, PATHINFO_EXTENSION);

    $type_file_yg_disetujui = ['xls', 'csv', 'xlsx'];

    // mengambil informasi jenis file yang hendak dimasukkan.
    if(in_array($info_path, $type_file_yg_disetujui)) {

        $ambil_input_file = $_FILES['input_import_excell']['tmp_name'];

        /** Load $inputFileName to a Spreadsheet object **/
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($ambil_input_file);
        $ambil_data = $spreadsheet->getActiveSheet()->toArray();

        // mengambil data dari dalam excell untuk di-importkan sesuai baris excell-nya
        $jumlah_ambil_data_bukan_headernya = 0;
        foreach($ambil_data as $data) {

            if($jumlah_ambil_data_bukan_headernya > 0) {

                $nama = pg_escape_string($data['0']);
                $alamat = pg_escape_string($data['1']);
                $anggaran = pg_escape_string($data['2']);
                $keterangan = pg_escape_string($data['3']);

                $memasukkan_data_ke_dalam_database = "INSERT INTO public.data_excell (nama, alamat, anggaran, keterangan) VALUES ('$nama', '$alamat', '$anggaran', '$keterangan')";
                $query_data = pg_query($conn, $memasukkan_data_ke_dalam_database);
                $msg = true;

            } else {

                $jumlah_ambil_data_bukan_headernya = "1";

            }
        }

        if(isset($msg)) {

            $_SESSION['message_success'] = 'File Berhasil Di-import';
            header('Location: index.php');
            exit(0);

        } else {

            $_SESSION['message'] = 'File Gagal Di-import';
            header('Location: index.php');
            exit(0);

        }
        
    } else {

        $_SESSION['message'] = 'File yang disetujui hanya xls, csv, xlsx';
        header('Location: index.php');
        exit(0);

    }

}


// Untuk Bagian Export
if(isset($_POST['export_data'])) {

    $ambil_request_type_export = pg_escape_string($_POST['select_export']);
    $ambil_isi_data = pg_query($conn, "SELECT * FROM public.data_excell");

    if(pg_num_rows($ambil_isi_data) > 0) {
        
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Nama');
        $sheet->setCellValue('B1', 'Alamat');
        $sheet->setCellValue('C1', 'Keterangan');
        $sheet->setCellValue('D1', 'Anggaran');

        // Dimulai dari 2 karena baris ke 1 untuk header bukan datanya
        $urutanExcell = 2;
        while($data = pg_fetch_object($ambil_isi_data)) {

            $sheet->getStyle('A1:D1')->getAlignment()->setHorizontal('center');

            foreach(range('A','D') as $columnID) {
                $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }

            $sheet->setCellValue('A'.$urutanExcell, $data->nama);
            $sheet->setCellValue('B'.$urutanExcell, $data->alamat);
            $sheet->setCellValue('C'.$urutanExcell, $data->keterangan);
            $sheet->setCellValue('D'.$urutanExcell, $data->anggaran);
            $urutanExcell++;
            $msg = true;

        }

        $nama_file = "hasil_exsport";

        // Ambil type file yang mau diexport
        if($ambil_request_type_export == 'Xlsx') {
            
            $writer = new Xlsx($spreadsheet);
            $nama_file_export = $nama_file.".xlsx";
            
        } elseif($ambil_request_type_export == 'Xls') {

            $writer = new Xls($spreadsheet);
            $nama_file_export = $nama_file.".xls";

        } elseif($ambil_request_type_export == 'Csv') {

            $writer = new Csv($spreadsheet);
            $nama_file_export = $nama_file.".csv";
            
        }
        
        
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'. urlencode($nama_file_export) .'" ');

        $writer->save("php://output");

    } else {

        $_SESSION['message'] = 'Data Kosong Table Kosong, Gagal Export';
        header('Location: index.php');
        exit(0);

    }

}